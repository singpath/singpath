/**
 * Singpath services.
 *
 * Note: if a service usage is limited to a component, it should be defined with the component.
 */
'use strict';

import './datastore.js';
import './routes.js';
